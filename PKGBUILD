# Maintainer: Stefano Capitani <stefanoatmanjarodotorg>
# Maintainer: Andreas Radke <andyrtr@archlinux.org>

_pkgname=cups-filters
pkgname=$_pkgname-driverless
pkgver=1.21.2
pkgrel=1
pkgdesc="OpenPrinting CUPS Filters"
arch=('x86_64')
url="https://wiki.linuxfoundation.org/openprinting/cups-filters"
license=('custom')
depends=('lcms2' 'poppler' 'qpdf' 'imagemagick' 'liblouis' 'ijs' 'libcups' 'systemd')
makedepends=('ghostscript' 'ttf-dejavu' 'python' 'mupdf-tools') # ttf-dejavu for make check
optdepends=('ghostscript: for non-PostScript printers to print with CUPS to convert PostScript to raster images'
	    'foomatic-db: drivers use Ghostscript to convert PostScript to a printable form directly'
	    'foomatic-db-engine: drivers use Ghostscript to convert PostScript to a printable form directly'
	    'foomatic-db-nonfree: drivers use Ghostscript to convert PostScript to a printable form directly'
	    'antiword: needed to convert MS Word documents (requires also docx2txt (AUR)')
conflicts=("$_pkgname")
provides=("$_pkgname=$pkgver")
backup=(etc/cups/cups-browsed.conf)
source=("https://www.openprinting.org/download/cups-filters/$_pkgname-$pkgver.tar.xz"
		'0001-utils-Enable-driverless-configuration-automagically.patch'
		'0002-utils-Fix-broken-systemd-unit.patch'
		'0001-Use-absolute-paths-for-font-directory-and-do-not-mak-v2.patch')
sha256sums=('2203060fd55406e5454b52a7b4bf8906a29a26fdc5777df75709443a5a78e09b'
            '8cdc7fc6e9baeb92dce2e459927b389db9eb6d1ae192a285901ae36f6048da54'
            'eea2c326b59e593a7a8f8fd789db4ca0fad581247d2f9e45c118d6dedf54cedb'
            '66e7c767e0a21ac9529484769bbe3fdfb32cb0ae8e3884dfa282bbf314fcb55c')

prepare() {
	cd $_pkgname-$pkgver
	#enabling driverless configuration
	#https://dev.solus-project.com/source/cups-filters/browse/master/files/
 	patch -Np1 -i ../0001-utils-Enable-driverless-configuration-automagically.patch
	patch -Np1 -i ../0002-utils-Fix-broken-systemd-unit.patch
	patch -Np1 -i ../0001-Use-absolute-paths-for-font-directory-and-do-not-mak-v2.patch
}

build() {
  cd $_pkgname-$pkgver
  ./configure --prefix=/usr  \
    --sysconfdir=/etc \
    --sbindir=/usr/bin \
    --localstatedir=/var \
    --without-rcdir \
    --disable-static \
    --disable-mutool \
    --enable-imagefilters \
    --enable-auto-setup-driverless \
    --with-fontdir=/usr/share/fonts/conf.d \
    --with-cups-rundir=/run/cups \
    --with-test-font-path=/usr/share/fonts/TTF/DejaVuSans.ttf \
    --enable-ijs --enable-opvp
  make
}

check() {
  cd $_pkgname-$pkgver
  make check
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="$pkgdir/" install
  
  # add upstream systemd support file
  install -Dm644 utils/cups-browsed.service ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  sed -i "s|/usr/sbin/cups-browsed|/usr/bin/cups-browsed|" ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  sed -i "s|cups.service|org.cups.cupsd.service|g" ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  
  # Enable to start on boot
  install -D -d -m 755 ${pkgdir}/usr/lib/systemd/system/multi-user.target.wants
  ln -sv ../cups-browsed.service ${pkgdir}/usr/lib/systemd/system/multi-user.target.wants/.
  
  # use cups group from cups pkg FS#56818
  chgrp -R 209 ${pkgdir}/etc/cups

  # license
  mkdir -p "${pkgdir}"/usr/share/licenses/${_pkgname}
  install -m644 "${srcdir}"/${_pkgname}-${pkgver}/COPYING "${pkgdir}"/usr/share/licenses/${_pkgname}/
}

